package com.retrixe.marriage;

import com.retrixe.marriage.commands.*;
import com.retrixe.marriage.storage.GsonStorage;
import com.retrixe.marriage.storage.Storage;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.server.command.CommandManager;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import org.jetbrains.annotations.NotNull;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;
import org.quiltmc.qsl.command.api.CommandRegistrationCallback;
import org.quiltmc.qsl.lifecycle.api.event.ServerTickEvents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;

public class Marriage implements ModInitializer, Executor {
	private static final Queue<Runnable> TASK_QUEUE = new ConcurrentLinkedQueue<>();
	public static final Logger LOGGER = LoggerFactory.getLogger("Marriage");
	public static final Text PREFIX = Text.literal("[").formatted(Formatting.AQUA)
			.append(Text.literal("Marriage").formatted(Formatting.DARK_AQUA))
			.append(Text.literal("] ").formatted(Formatting.AQUA));
	public static Executor EXECUTOR = null;
	public static Storage STORAGE = null;

	// TODO: chat formatting (with genders!)
	// TODO: player name cache backed by Storage interface
	// TODO: ability to toggle PvP on and off (return of the mixin)
	@Override
	public void onInitialize(ModContainer mod) {
		EXECUTOR = this;
		STORAGE = new GsonStorage();
		STORAGE.initialise();
		CommandRegistrationCallback.EVENT.register((dispatcher, dedicated, registrationEnvironment) ->
				dispatcher.register(CommandManager.literal("marry")
						.then(CommandManager.argument("player", EntityArgumentType.players()).executes(new MarryPlayerCommand()))
						.then(CommandManager.literal("list").executes(new MarryListCommand()))
						.then(CommandManager.literal("help").executes(new MarryHelpCommand()))
						.then(CommandManager.literal("divorce").executes(new MarryDivorceCommand()))
						.then(CommandManager.literal("home").executes(new MarryHomeCommand()))
						.then(CommandManager.literal("sethome").executes(new MarrySetHomeCommand()))
						.then(CommandManager.literal("tp").executes(new MarryTeleportCommand()))
						.then(CommandManager.literal("teleport").executes(new MarryTeleportCommand()))
						.then(CommandManager.literal("heal").executes(new MarryHealCommand()))
						.then(CommandManager.literal("gift").executes(new MarryGiftCommand()))
						.executes(new MarryHelpCommand())
		));
		ServerTickEvents.END.register(server -> {
			var runnable = TASK_QUEUE.poll();
			while (runnable != null) {
				try {
					runnable.run();
				} catch (Exception e) {
					LOGGER.error("An unknown error occurred in the main thread scheduler!", e);
				}
				runnable = TASK_QUEUE.poll();
			}
		});
	}

	@Override
	public void execute(@NotNull Runnable runnable) {
		TASK_QUEUE.add(runnable);
	}
}
