package com.retrixe.marriage.storage;

import com.google.gson.*;
import com.retrixe.marriage.Marriage;
import com.retrixe.marriage.data.Location;
import com.retrixe.marriage.data.MarriageInfo;
import net.minecraft.util.Pair;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GsonStorage implements Storage {
    private final ExecutorService fileThread = Executors.newSingleThreadExecutor();
    private final Gson gson = new Gson();
    private File storageFile;

    private record StorageFile(int version, Map<UUID, MarriageInfo> marriages) {}

    @Override
    public void initialise() {
        File configDir = Path.of("config").toAbsolutePath().toFile();
        if (!configDir.isDirectory() && configDir.exists()) {
            Marriage.LOGGER.warn("A file was found in place of the config folder!");
        } else if (!configDir.isDirectory() && !configDir.mkdir()) {
            Marriage.LOGGER.warn("Failed to create config folder! This may cause errors!");
        }
        configDir = new File(configDir, "marriage");
        if (!configDir.isDirectory() && configDir.exists()) {
            Marriage.LOGGER.warn("A file was found in place of the config/marriage folder!");
        } else if (!configDir.isDirectory() && !configDir.mkdir()) {
            Marriage.LOGGER.warn("Failed to create config/marriage folder! This may cause errors!");
        }
        storageFile = new File(configDir, "storage.json");
    }

    // Why not supplyAsync? Because checked exceptions. :v
    @Override
    public CompletableFuture<List<MarriageInfo>> getAllMarriages() {
        CompletableFuture<List<MarriageInfo>> future = new CompletableFuture<>();
        fileThread.submit(() -> {
            try {
                future.complete(readJsonFile().marriages().values().stream().toList());
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        });
        return future;
    }

    @Override
    public CompletableFuture<MarriageInfo> getMarriageInfoOf(UUID player) {
        CompletableFuture<MarriageInfo> future = new CompletableFuture<>();
        fileThread.submit(() -> {
            try {
                future.complete(readJsonFile().marriages().values().stream()
                        .filter(marriage ->
                                marriage.partners().getLeft().compareTo(player) == 0 ||
                                        marriage.partners().getRight().compareTo(player) == 0)
                        .findFirst()
                        .orElse(null));
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        });
        return future;
    }

    @Override
    public CompletableFuture<MarriageInfo> createMarriage(UUID player1, UUID player2) {
        CompletableFuture<MarriageInfo> future = new CompletableFuture<>();
        fileThread.submit(() -> {
            try {
                MarriageInfo marriageInfo = new MarriageInfo(
                        UUID.randomUUID(),
                        new Pair<>(player1, player2),
                        null
                );
                StorageFile storage = readJsonFile();
                storage.marriages().put(marriageInfo.id(), marriageInfo);
                writeJsonFile(storage);
                future.complete(marriageInfo);
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        });
        return future;
    }

    @Override
    public CompletableFuture<Boolean> removeMarriageByUUID(UUID marriage) {
        CompletableFuture<Boolean> future = new CompletableFuture<>();
        fileThread.submit(() -> {
            try {
                StorageFile storage = readJsonFile();
                MarriageInfo removed = storage.marriages().remove(marriage);
                writeJsonFile(storage);
                future.complete(removed == null);
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        });
        return future;
    }

    @Override
    public CompletableFuture<Boolean> removeMarriageByPlayer(UUID player) {
        CompletableFuture<Boolean> future = new CompletableFuture<>();
        fileThread.submit(() -> {
            try {
                StorageFile storage = readJsonFile();
                boolean removed = storage.marriages().values().removeIf(marriage ->
                        marriage.partners().getLeft().compareTo(player) == 0 ||
                                marriage.partners().getRight().compareTo(player) == 0);
                writeJsonFile(storage);
                future.complete(removed);
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        });
        return future;
    }

    @Override
    public CompletableFuture<MarriageInfo> setMarriageHome(UUID marriage, Location home) {
        CompletableFuture<MarriageInfo> future = new CompletableFuture<>();
        fileThread.submit(() -> {
            try {
                StorageFile storage = readJsonFile();
                MarriageInfo oldMarriage = storage.marriages.get(marriage);
                if (oldMarriage == null) {
                    future.complete(null);
                } else {
                    MarriageInfo newMarriage = oldMarriage.withHome(home);
                    storage.marriages().put(marriage, newMarriage);
                    writeJsonFile(storage);
                    future.complete(newMarriage);
                }
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        });
        return future;
    }

    /*
    {
      version: 1,
      marriages: {
        [couple_uuid]: { home: "world 0 0 0 0 0", partners: [uuid1, uuid2] }
      }
    }
    */
    private StorageFile readJsonFile() throws IOException {
        JsonObject json = new JsonObject();
        if (storageFile.exists()) {
            json = JsonParser
                    .parseString(new String(Files.readAllBytes(storageFile.toPath())))
                    .getAsJsonObject();
        }
        // version
        int version = 1;
        JsonElement versionElement = json.get("version");
        if (versionElement != null &&
                versionElement.isJsonPrimitive() &&
                versionElement.getAsJsonPrimitive().isNumber()) {
            version = versionElement.getAsInt();
        }
        // marriages
        HashMap<UUID, MarriageInfo> marriages = new HashMap<>();
        JsonElement marriagesElement = json.get("marriages");
        if (marriagesElement != null && marriagesElement.isJsonObject()) {
            for (Map.Entry<String, JsonElement> entry : marriagesElement.getAsJsonObject().entrySet()) {
                JsonObject marriage = entry.getValue().getAsJsonObject();
                UUID id = UUID.fromString(entry.getKey());
                JsonElement home = marriage.get("home");
                marriages.put(id, new MarriageInfo(
                        id,
                        new Pair<>(
                                UUID.fromString(marriage.getAsJsonArray("partners").get(0).getAsString()),
                                UUID.fromString(marriage.getAsJsonArray("partners").get(1).getAsString())
                        ),
                        home != null ? Location.parseString(home.getAsString()) : null
                ));
            }
        }
        return new StorageFile(version, marriages);
    }

    private void writeJsonFile(StorageFile storage) throws IOException {
        var json = new JsonObject();
        // version
        json.addProperty("version", 1);
        // marriages
        var marriagesJson = new JsonObject();
        storage.marriages().forEach((key, value) -> {
            var marriageJson = new JsonObject();
            if (value.home() != null) marriageJson.addProperty("home", value.home().toString());
            var partners = new JsonArray();
            partners.add(value.partners().getLeft().toString());
            partners.add(value.partners().getRight().toString());
            marriageJson.add("partners", partners);
            marriagesJson.add(value.id().toString(), marriageJson);
        });
        json.add("marriages", marriagesJson);
        Files.writeString(storageFile.toPath(), gson.toJson(json));
    }
}
