package com.retrixe.marriage.storage;

import com.retrixe.marriage.data.Location;
import com.retrixe.marriage.data.MarriageInfo;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface Storage {
    void initialise();
    CompletableFuture<List<MarriageInfo>> getAllMarriages(); // TODO: pagination
    CompletableFuture<MarriageInfo> getMarriageInfoOf(UUID player);
    CompletableFuture<MarriageInfo> createMarriage(UUID player1, UUID player2);
    CompletableFuture<Boolean> removeMarriageByUUID(UUID marriage);
    CompletableFuture<Boolean> removeMarriageByPlayer(UUID player);
    CompletableFuture<MarriageInfo> setMarriageHome(UUID marriage, Location home);
}
