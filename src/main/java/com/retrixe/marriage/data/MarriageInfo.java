package com.retrixe.marriage.data;

import net.minecraft.util.Pair;

import java.util.UUID;

public record MarriageInfo(UUID id, Pair<UUID, UUID> partners, Location home) {
    public MarriageInfo withHome(Location home) {
        return new MarriageInfo(id, partners, home);
    }
}
