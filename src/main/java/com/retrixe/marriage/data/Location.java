package com.retrixe.marriage.data;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.level.ServerWorldProperties;

public record Location(String world, double x, double y, double z, float yaw, float pitch) {
    public Location(ServerWorld world, double x, double y, double z, float yaw, float pitch) {
        this(getServerWorldAsString(world), x, y, z, yaw, pitch);
    }

    @Override
    public String toString() {
        return world + " " + x + " " + y + " " + z + " " + yaw + " " + pitch;
    }

    public ServerWorld getServerWorld(MinecraftServer server) {
        for (ServerWorld serverWorld : server.getWorlds()) {
            if (world.equals(getServerWorldAsString(serverWorld))) {
                return serverWorld;
            }
        }
        return null;
    }

    public static Location parseString(String location) {
        String[] split = location.split(" ");
        try {
            return new Location(
                    split[0],
                    Double.parseDouble(split[1]),
                    Double.parseDouble(split[2]),
                    Double.parseDouble(split[3]),
                    Float.parseFloat(split[4]),
                    Float.parseFloat(split[5])
            );
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static String getServerWorldAsString(ServerWorld serverWorld) { // TODO: is there a better way?
        String name = "world";
        if (serverWorld.getLevelProperties() instanceof ServerWorldProperties properties) {
            name = properties.getLevelName();
        }
        String dimension = serverWorld.getRegistryKey().getValue().toString();
        return name + ":" + dimension;
    }
}
