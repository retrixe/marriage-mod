package com.retrixe.marriage.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.retrixe.marriage.Marriage;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

public class MarryHealCommand implements Command<ServerCommandSource> {
    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        var player = context.getSource().getPlayer();
        Marriage.STORAGE.getMarriageInfoOf(player.getUuid()).thenAcceptAsync(marriage -> {
            if (marriage == null) {
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("lol u're an unmarried virgin").formatted(Formatting.RED)));
            } else {
                var partnerUuid = marriage.partners().getLeft().equals(player.getUuid())
                        ? marriage.partners().getRight()
                        : marriage.partners().getLeft();
                var partner = context.getSource().getServer().getPlayerManager()
                        .getPlayer(partnerUuid);
                if (partner == null || partner.isDisconnected()) {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("Your spouse is not online!").formatted(Formatting.RED)));
                } else {
                    float toHeal = partner.getMaxHealth() - partner.getHealth();
                    float spareHealth = player.getHealth() - 1;
                    float healthToRemain = spareHealth - toHeal;
                    if (healthToRemain < 0)
                        healthToRemain = 0;
                    player.setHealth(1 + healthToRemain);
                    partner.setHealth(partner.getHealth() + (healthToRemain == 0 ? spareHealth : toHeal));

                    partner.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("Your spouse has healed you!").formatted(Formatting.DARK_AQUA)));
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("You have healed your spouse!").formatted(Formatting.DARK_AQUA)));
                }
            }
        }, Marriage.EXECUTOR).exceptionallyAsync(throwable -> {
            Marriage.LOGGER.error("An error occurred!", throwable);
            player.sendSystemMessage(Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)));
            return null;
        }, Marriage.EXECUTOR);
        return 0;
    }
}
