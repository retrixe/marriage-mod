package com.retrixe.marriage.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.retrixe.marriage.Marriage;
import com.retrixe.marriage.data.Location;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

public class MarrySetHomeCommand implements Command<ServerCommandSource> {
    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        var player = context.getSource().getPlayer();
        // TODO: safety check
        var location = new Location(
                player.getWorld(), player.getX(), player.getY(), player.getZ(), player.getYaw(), player.getPitch());

        Marriage.STORAGE.getMarriageInfoOf(player.getUuid()).thenAcceptAsync(marriage -> {
            if (marriage == null) {
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("lol u're an unmarried virgin").formatted(Formatting.RED)));
                return;
            }
            Marriage.STORAGE.setMarriageHome(marriage.id(), location).thenAcceptAsync(m -> {
                if (m == null) {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("lol u're an unmarried virgin").formatted(Formatting.RED)));
                } else {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("Your home has been set successfully!")
                                    .formatted(Formatting.DARK_AQUA)));
                }
            }, Marriage.EXECUTOR).exceptionallyAsync(t -> {
                Marriage.LOGGER.error("An error occurred!", t);
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)));
                return null;
            }, Marriage.EXECUTOR);
        }, Marriage.EXECUTOR).exceptionallyAsync(t -> {
            Marriage.LOGGER.error("An error occurred!", t);
            player.sendSystemMessage(Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)));
            return null;
        }, Marriage.EXECUTOR);
        return 0;
    }
}
