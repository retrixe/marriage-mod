package com.retrixe.marriage.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.retrixe.marriage.Marriage;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

public class MarryTeleportCommand implements Command<ServerCommandSource> {
    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        var player = context.getSource().getPlayer();
        Marriage.STORAGE.getMarriageInfoOf(player.getUuid()).thenAcceptAsync(marriage -> {
            if (marriage == null) {
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("lol u're an unmarried virgin").formatted(Formatting.RED)));
            } else {
                var partnerUuid = marriage.partners().getLeft().equals(player.getUuid())
                        ? marriage.partners().getRight()
                        : marriage.partners().getLeft();
                var partner = context.getSource().getServer().getPlayerManager()
                        .getPlayer(partnerUuid);
                if (partner == null || partner.isDisconnected()) {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("Your spouse is not online!").formatted(Formatting.RED)));
                } else {
                    // TODO: safety check
                    player.teleport(partner.getWorld(),
                            partner.getX(),
                            partner.getY(),
                            partner.getZ(),
                            partner.getYaw(),
                            partner.getPitch());
                    partner.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("Your spouse has teleported to you!")
                                    .formatted(Formatting.DARK_AQUA)));
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("You have been teleported to your spouse!")
                                    .formatted(Formatting.DARK_AQUA)));
                }
            }
        }, Marriage.EXECUTOR).exceptionallyAsync(throwable -> {
            Marriage.LOGGER.error("An error occurred!", throwable);
            player.sendSystemMessage(Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)));
            return null;
        }, Marriage.EXECUTOR);
        return 0;
    }
}
