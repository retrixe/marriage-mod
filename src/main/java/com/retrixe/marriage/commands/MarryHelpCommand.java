package com.retrixe.marriage.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.retrixe.marriage.Marriage;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import static net.minecraft.util.Formatting.*;

public class MarryHelpCommand implements Command<ServerCommandSource> {
    private final Text DASH = Text.literal(" - ").formatted();
    private final Text HELP = Text.literal("")
            .append(Text.literal("List of commands:\n").formatted(DARK_AQUA))
            .append(Text.literal("/marry <player>").formatted(AQUA)).append(DASH).append(Text.literal("Propose or accept marriage to a player.\n").formatted(DARK_AQUA))
            .append(Text.literal("/marry list    ").formatted(AQUA)).append(DASH).append(Text.literal("Get a list of all marriages in the server.\n").formatted(DARK_AQUA))
            .append(Text.literal("/marry help    ").formatted(AQUA)).append(DASH).append(Text.literal("Show this help message.\n").formatted(DARK_AQUA))
            .append(Text.literal("/marry divorce ").formatted(AQUA)).append(DASH).append(Text.literal("End your marriage with another player.\n").formatted(DARK_AQUA))
            .append(Text.literal("/marry home    ").formatted(AQUA)).append(DASH).append(Text.literal("Teleport to your marriage home.\n").formatted(DARK_AQUA))
            .append(Text.literal("/marry sethome ").formatted(AQUA)).append(DASH).append(Text.literal("Set the location of your marriage home.\n").formatted(DARK_AQUA))
            .append(Text.literal("/marry tp      ").formatted(AQUA)).append(DASH).append(Text.literal("Teleport to your spouse.\n").formatted(DARK_AQUA))
            .append(Text.literal("/marry gift    ").formatted(AQUA)).append(DASH).append(Text.literal("Gift item in your hand to your spouse.\n").formatted(DARK_AQUA))
            .append(Text.literal("/marry heal    ").formatted(AQUA)).append(DASH).append(Text.literal("Heal your spouse with your own health.").formatted(DARK_AQUA));

    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        if (context.getSource().getEntity() == null) { // console
            context.getSource().sendFeedback(Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(HELP), false);
        } else {
            var player = context.getSource().getPlayer();
            Marriage.STORAGE.getMarriageInfoOf(player.getUuid()).thenAcceptAsync(marriage -> {
                String status = "Unmarried (loooooool virgin)";
                if (marriage != null) {
                    var partnerUuid = marriage.partners().getLeft().equals(player.getUuid())
                            ? marriage.partners().getRight()
                            : marriage.partners().getLeft();
                    var partner = context.getSource().getServer().getPlayerManager().getPlayer(partnerUuid);
                    status = "Married to " + (partner == null ? partnerUuid.toString() : partner.getName().getString());
                }
                context.getSource().sendFeedback(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("\n"))
                        .append(Text.literal("Status: ").formatted(Formatting.AQUA))
                        .append(Text.literal(status).formatted(Formatting.DARK_AQUA))
                        .append(Text.literal("\n\n"))
                        .append(HELP), false);
            }, Marriage.EXECUTOR).exceptionallyAsync(throwable -> {
                Marriage.LOGGER.error("An error occurred!", throwable);
                context.getSource().sendFeedback(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)),
                        false);
                return null;
            }, Marriage.EXECUTOR);
        }
        return 0;
    }
}
