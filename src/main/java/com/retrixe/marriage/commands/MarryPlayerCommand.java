package com.retrixe.marriage.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.retrixe.marriage.Marriage;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MarryPlayerCommand implements Command<ServerCommandSource> {
    // TODO: proposals should expire
    private final Map<UUID, UUID> proposals = new HashMap<>(); // key - proposer, value - proposed to

    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        var player = context.getSource().getPlayer();
        var target = EntityArgumentType.getPlayer(context, "player");
        Marriage.STORAGE.getMarriageInfoOf(player.getUuid()).thenAcceptAsync(marriageInfo -> {
            if (marriageInfo != null) {
                UUID partner = marriageInfo.partners().getLeft().equals(player.getUuid())
                        ? marriageInfo.partners().getRight()
                        : marriageInfo.partners().getLeft();
                if (target.getUuid().equals(partner)) {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("you're already married to this person, dumbass")
                                    .formatted(Formatting.RED)));
                } else {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("cheating, huh? ").formatted(Formatting.RED))
                            .append(Text.literal("/marry divorce").formatted(Formatting.AQUA))
                            .append(Text.literal(" first").formatted(Formatting.RED)));
                    // notify the partner
                    var p = context.getSource().getServer().getPlayerManager().getPlayer(partner);
                    if (p != null && !p.isDisconnected()) {
                        p.sendSystemMessage(Text.literal("")
                                .append(Marriage.PREFIX)
                                .append(Text.literal("your partner tried to cheat on you with ")
                                        .formatted(Formatting.RED))
                                .append(Text.literal(target.getName().getString()).formatted(Formatting.AQUA)));
                    }
                }
            } else if (player.equals(target)) {
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("nice try lol youre an eternal virgin").formatted(Formatting.RED)));
            } else if (proposals.containsKey(player.getUuid()) &&
                    proposals.get(player.getUuid()).equals(target.getUuid())) {
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("You have already proposed to " + target.getName().getString() + "!")
                                .formatted(Formatting.RED)));
            } else if (proposals.containsKey(target.getUuid()) &&
                    proposals.get(target.getUuid()).equals(player.getUuid())) {
                proposals.remove(target.getUuid());
                Marriage.STORAGE.createMarriage(target.getUuid(), player.getUuid()).thenAcceptAsync(
                        marriage -> context.getSource().getServer()
                                .getPlayerManager()
                                .getPlayerList()
                                .forEach(p -> p.sendSystemMessage(Text.literal("")
                                        .append(Marriage.PREFIX)
                                        .append(Text.literal(target.getName().getString() + " and " +
                                                        player.getName().getString() +
                                                        " have just married!")
                                                .formatted(Formatting.GREEN))
                                )),
                        Marriage.EXECUTOR
                ).exceptionallyAsync(throwable1 -> {
                    Marriage.LOGGER.error("An error occurred!", throwable1);
                    context.getSource().sendFeedback(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)), false);
                    return null;
                }, Marriage.EXECUTOR);
            } else {
                proposals.put(player.getUuid(), target.getUuid());
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("You have proposed to ").formatted(Formatting.DARK_AQUA))
                        .append(Text.literal(target.getName().getString()).formatted(Formatting.AQUA))
                        .append(Text.literal("!").formatted(Formatting.DARK_AQUA)));
                target.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal(player.getName().getString()).formatted(Formatting.AQUA))
                        .append(Text.literal(" has proposed to you! Run ").formatted(Formatting.DARK_AQUA))
                        .append(Text.literal("/marry " + player.getName().getString()).formatted(Formatting.AQUA))
                        .append(Text.literal(" to accept!").formatted(Formatting.DARK_AQUA)));
            }
        }, Marriage.EXECUTOR).exceptionallyAsync(throwable -> {
            Marriage.LOGGER.error("An error occurred!", throwable);
            context.getSource().sendFeedback(Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)), false);
            return null;
        }, Marriage.EXECUTOR);
        return 0;
    }
}
