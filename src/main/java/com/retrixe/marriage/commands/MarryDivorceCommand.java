package com.retrixe.marriage.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.retrixe.marriage.Marriage;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.util.UUID;

public class MarryDivorceCommand implements Command<ServerCommandSource> {
    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        var player = context.getSource().getPlayer();
        Marriage.STORAGE.getMarriageInfoOf(player.getUuid()).thenAcceptAsync(marriage -> {
            if (marriage == null) {
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("lol you virgin ur not even married").formatted(Formatting.RED)));
                return;
            }
            Marriage.STORAGE.removeMarriageByUUID(marriage.id()).thenAcceptAsync(removed -> {
                if (removed) {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("You have divorced your spouse!").formatted(Formatting.RED)));
                    UUID partner = marriage.partners().getLeft().equals(player.getUuid())
                            ? marriage.partners().getRight()
                            : marriage.partners().getLeft();
                    // notify the partner
                    var p = context.getSource().getServer().getPlayerManager().getPlayer(partner);
                    if (p != null && !p.isDisconnected()) {
                        p.sendSystemMessage(Text.literal("")
                                .append(Marriage.PREFIX)
                                .append(Text.literal("Your spouse has divorced you!").formatted(Formatting.RED)));
                    }
                } else {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("lol you virgin ur not even married").formatted(Formatting.RED)));
                }
            }, Marriage.EXECUTOR).exceptionallyAsync(throwable -> {
                Marriage.LOGGER.error("An error occurred!", throwable);
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)));
                return null;
            }, Marriage.EXECUTOR);
        }, Marriage.EXECUTOR).exceptionallyAsync(throwable -> {
            Marriage.LOGGER.error("An error occurred!", throwable);
            player.sendSystemMessage(Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)));
            return null;
        }, Marriage.EXECUTOR);
        return 0;
    }
}
