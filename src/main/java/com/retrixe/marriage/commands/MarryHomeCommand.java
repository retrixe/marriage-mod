package com.retrixe.marriage.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.retrixe.marriage.Marriage;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

public class MarryHomeCommand implements Command<ServerCommandSource> {
    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        var player = context.getSource().getPlayer();
        Marriage.STORAGE.getMarriageInfoOf(player.getUuid()).thenAcceptAsync(marriage -> {
            if (marriage == null) {
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("lol u're an unmarried virgin").formatted(Formatting.RED)));
            } else if (marriage.home() == null) {
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("You do not have a home set! Use ").formatted(Formatting.RED))
                        .append(Text.literal("/marry sethome").formatted(Formatting.AQUA))
                        .append(Text.literal(" to set your marriage home.").formatted(Formatting.RED)));
            } else {
                var world = marriage.home().getServerWorld(context.getSource().getServer());
                if (world == null) {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("The home world could not be found!").formatted(Formatting.RED)));
                } else {
                    var home = marriage.home();
                    player.teleport(world, home.x(), home.y(), home.z(), home.yaw(), home.pitch());
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("You have been teleported to home!").formatted(Formatting.DARK_AQUA)));
                }
            }
        }, Marriage.EXECUTOR).exceptionallyAsync(throwable -> {
            Marriage.LOGGER.error("An error occurred!", throwable);
            player.sendSystemMessage(Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)));
            return null;
        }, Marriage.EXECUTOR);
        return 0;
    }
}
