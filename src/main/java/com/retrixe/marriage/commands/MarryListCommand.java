package com.retrixe.marriage.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.retrixe.marriage.Marriage;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

public class MarryListCommand implements Command<ServerCommandSource> {
    @Override
    public int run(CommandContext<ServerCommandSource> context) {
        Marriage.STORAGE.getAllMarriages().thenAcceptAsync(marriageInfos -> {
            MutableText text = Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(Text.literal("List of marriages:\n").formatted(Formatting.DARK_AQUA));
            for (int i = 0; i < marriageInfos.size(); i++) {
                var marriage = marriageInfos.get(i);
                var player1 = context.getSource().getServer().getPlayerManager()
                        .getPlayer(marriage.partners().getLeft());
                var player2 = context.getSource().getServer().getPlayerManager()
                        .getPlayer(marriage.partners().getRight());
                var name1 = player1 == null
                        ? marriage.partners().getLeft().toString()
                        : player1.getName().getString();
                var name2 = player2 == null
                        ? marriage.partners().getRight().toString()
                        : player2.getName().getString();
                text = text
                        .append(Text.literal(name1).formatted(Formatting.AQUA))
                        .append(Text.literal(" - ").formatted())
                        .append(Text.literal(name2).formatted(Formatting.AQUA));
                if (i < marriageInfos.size() - 1) text = text.append(Text.literal("\n"));
            }
            MutableText finalText = text;
            context.getSource().sendFeedback(finalText, false);
        }, Marriage.EXECUTOR).exceptionallyAsync(throwable -> {
            Marriage.LOGGER.error("An error occurred!", throwable);
            context.getSource().sendFeedback(Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)), false);
            return null;
        }, Marriage.EXECUTOR);
        return 0;
    }
}
