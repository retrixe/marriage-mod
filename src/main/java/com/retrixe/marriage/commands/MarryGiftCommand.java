package com.retrixe.marriage.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.retrixe.marriage.Marriage;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

public class MarryGiftCommand implements Command<ServerCommandSource> {
    @Override
    public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        var player = context.getSource().getPlayer();
        Marriage.STORAGE.getMarriageInfoOf(player.getUuid()).thenAcceptAsync(marriage -> {
            if (marriage == null) {
                player.sendSystemMessage(Text.literal("")
                        .append(Marriage.PREFIX)
                        .append(Text.literal("lol u're an unmarried virgin").formatted(Formatting.RED)));
            } else {
                var partnerUuid = marriage.partners().getLeft().equals(player.getUuid())
                        ? marriage.partners().getRight()
                        : marriage.partners().getLeft();
                var partner = context.getSource().getServer().getPlayerManager()
                        .getPlayer(partnerUuid);
                if (partner == null || partner.isDisconnected()) {
                    player.sendSystemMessage(Text.literal("")
                            .append(Marriage.PREFIX)
                            .append(Text.literal("Your spouse is not online!").formatted(Formatting.RED)));
                } else {
                    ItemStack itemInHand = player.getInventory().getMainHandStack();
                    if (itemInHand == null || itemInHand.isEmpty()) {
                        player.sendSystemMessage(Text.literal("")
                                .append(Marriage.PREFIX)
                                .append(Text.literal("You do not have an item in your hand!").formatted(Formatting.RED)));
                    } else {
                        String name = itemInHand.getTranslationKey();
                        int count = itemInHand.getCount();
                        player.getInventory().setStack(player.getInventory().selectedSlot, ItemStack.EMPTY);
                        partner.getInventory().insertStack(itemInHand);
                        partner.sendSystemMessage(Text.literal("")
                                .append(Marriage.PREFIX)
                                .append(Text.literal("You have been gifted ").formatted(Formatting.DARK_AQUA))
                                .append(Text.literal(String.valueOf(count)).formatted(Formatting.AQUA))
                                .append(Text.literal(" of ").formatted(Formatting.DARK_AQUA))
                                .append(Text.translatable(name).formatted(Formatting.AQUA))
                                .append(Text.literal(" by your partner!").formatted(Formatting.DARK_AQUA)));
                        player.sendSystemMessage(Text.literal("")
                                .append(Marriage.PREFIX)
                                .append(Text.literal("You have sent ").formatted(Formatting.DARK_AQUA))
                                .append(Text.literal(String.valueOf(count)).formatted(Formatting.AQUA))
                                .append(Text.literal(" of ").formatted(Formatting.DARK_AQUA))
                                .append(Text.translatable(name).formatted(Formatting.AQUA))
                                .append(Text.literal(" to your partner as a gift!").formatted(Formatting.DARK_AQUA)));
                    }
                }
            }
        }, Marriage.EXECUTOR).exceptionallyAsync(throwable -> {
            Marriage.LOGGER.error("An error occurred!", throwable);
            player.sendSystemMessage(Text.literal("")
                    .append(Marriage.PREFIX)
                    .append(Text.literal("An unknown error occurred.").formatted(Formatting.RED)));
            return null;
        }, Marriage.EXECUTOR);
        return 0;
    }
}
